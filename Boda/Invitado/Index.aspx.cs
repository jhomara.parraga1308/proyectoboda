﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Boda_Invitado_Index : System.Web.UI.Page
{
    readonly SqlConnection con =
        new SqlConnection(
            ConfigurationManager.
            ConnectionStrings["conexion"].ConnectionString);
    private object gvInvitados;

    protected void Page_Load(object sender, EventArgs e)
    {
        cargarDatos();
    }

    void cargarDatos()
    {
        SqlCommand comando =
            new SqlCommand("sp_consultar_invitados", con);
        comando.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataAdapter adaptador = new SqlDataAdapter(comando);
        DataTable tabla = new DataTable();
        adaptador.Fill(tabla);
        gvBod.DataSource = tabla;
        gvBod.DataBind();
        con.Close();
    }

}