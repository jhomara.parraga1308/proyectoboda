﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Boda_Servicio_Index : System.Web.UI.Page
{
    readonly SqlConnection con =
        new SqlConnection(
            ConfigurationManager.
            ConnectionStrings["conexion"].ConnectionString);
    private object gvSer;

    protected void Page_Load(object sender, EventArgs e)
    {
        cargarDatos();
    }

    void cargarDatos()
    {
        SqlCommand comando =
            new SqlCommand("sp_consultar_servicios", con);
        comando.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataAdapter adaptador = new SqlDataAdapter(comando);
        DataTable tabla = new DataTable();
        adaptador.Fill(tabla);
        gvServicio.DataSource = tabla;
        gvServicio.DataBind();
        con.Close();
    }

}